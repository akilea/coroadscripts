import pandas as pd

def extract_borne(ce_file,str_filter_out,borne_file):
    df = pd.read_csv (ce_file
        ,low_memory=False
        ,delimiter=",")

    df = df[["Nom de la borne de recharge","Niveau de recharge","Latitude","Longitude"]]
    if str_filter_out:
         df = df.loc[df["Niveau de recharge"] != str_filter_out]
    
    df['Niveau'] = "L 3"
    df['LatLon'] = df.apply(lambda row: ("("+str(row["Latitude"]) +","+ str(row["Longitude"])+")"), axis=1)
    df["Nom de la borne de recharge"] = df.apply(lambda row: str(row["Nom de la borne de recharge"]).replace(" ","_"), axis=1)
    df = df[["Nom de la borne de recharge","Niveau","LatLon"]]
    print(df.tail())
    df.to_csv(borne_file,sep="\t",header=False,index=False)

extract_borne("export_sites_fr.csv","","bornes-ce-L2L3.txt")
extract_borne("export_sites_fr.csv","Niveau 2","bornes-ce-L3.txt")