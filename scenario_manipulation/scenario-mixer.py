import re
import os
import pandas as pd
import numpy as np
from random import randint


def shuffle(data,destFile):
    reduced_df = data.sample(1500)
    sorted_df = reduced_df.sort_values(by=[4])
    print(sorted_df.head())
    sorted_df.to_csv(destFile,sep=" ",header=False,index=False)

def loadAllScenario(root):
    data = None
    for path, subdirs, files in os.walk(root):
        for name in files:
            #print(os.path.join(path, name))
            current_data = pd.read_csv(os.path.join(path, name), header=None, delimiter=r"\s+")
            if data is not None:
                data = data.append(current_data)
            else:
                data = current_data
    #data = data.rename(columns={0: "Nom", 1: "Depart", 2: "Arrivee",3:"Autonomie",4:"TempsDepart",5:"Autonomie"})
    #Preprocessing
    data[3] = 0 #Autonomie
    data[5] = 0 #Temps charge
    data[4] = data[4].apply(lambda x: randint(0,7200))  #Temps départ sur 2h
    data[0] = np.arange(data.shape[0])
    data = data.sort_values(by=[4])
    print(data.head())
    return data

def mixt(sourceFolder,destinationFolder,amount):
    sc = loadAllScenario(sourceFolder)
    for i in range(0,amount):
        shuffle(sc,destinationFolder+"/Scenario"+str(i)+".txt")

mixt("scenario_manipulation/Route_montreal_to_matane_and__to_","scenario_manipulation/ScenarioA",10)
mixt("scenario_manipulation/Route_montreal_to_matane_and_megantic_to_saguenay","scenario_manipulation/ScenarioB",10)