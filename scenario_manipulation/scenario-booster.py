import re
import os

def gatherScenario(filePath):
    lines = []
    with open(filePath,"r") as f:
        lines = f.readlines()
    return lines

def boosts(filePath,lines,duplicateNames):
    with open(filePath,"w") as f:
        for l in lines:
            for d in duplicateNames:
                n = l.replace("Vehicle_", "Vehicle_"+d)
                f.write(n)

def createBoostFolder(nameFolderReference, nameNewFolder,duplicateNames):
    scenarioFilename = "scenario.txt"
    amountScenario = 10
    for i in range(1,amountScenario+1):
        currentFileReference = nameFolderReference+"/"+"No"+str(i)+"/"+scenarioFilename
        currentFolderDestination = nameNewFolder+"/"+"No"+str(i)
        lines = gatherScenario(currentFileReference)
        try:
            os.makedirs(currentFolderDestination)
        except OSError:
            print ("Creation of the directory %s failed" % currentFolderDestination)
        else:
            print ("Successfully created the directory %s " % currentFolderDestination)
        finalPath = currentFolderDestination + "/" + scenarioFilename
        boosts(finalPath,lines,duplicateNames)

def boostOne(baseFolder):
    dup500 = ["","A"]
    dup1000 = ["","A","B","C"]
    dup2000 = ["","A","B","C","D","E","F","G"]
    ref250 = "Route_montreal_to_quebec/Voiture250"
    createBoostFolder(ref250,baseFolder+"/Voiture500" ,dup500)
    createBoostFolder(ref250,baseFolder+"/Voiture1000" ,dup1000)
    createBoostFolder(ref250,baseFolder+"/Voiture2000" ,dup2000)

boostOne("Route_montreal_to_quebec")