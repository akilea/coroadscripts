from plot_builder_seaborn import *
from plot_exporter import *
from booster import booster
import matplotlib.pyplot as plt
import matplotlib as mpl
#mpl.rcParams['lines.markersize'] = 30

def m(dictA,dictB):
    return {**dictA , **dictB}

class Plotter:
    def __init__(self,file_to_loadA,file_to_loadB,print_df = False):
        self.simulation_time_convert = 1.0/3600.0
        self.performance_time_convert = 1.0

        boostA = booster.Booster()
        boostA.load(file_to_loadA)
        boostA.boostA()
        boostB = booster.Booster()
        boostB.load(file_to_loadB)
        boostB.boostB()

        df = self.preprocess(boostA.df,boostB.df)
        if print_df:
            print(df.head())

        preprocess_extrema = self.preprocess_extrema(df)

        self.exporter = PlotExporter(preview = False,overleaf_image = True,overleaf_folder = "overleaf_plot")
        self.builder = PlotBuilderSeaBorn(self.exporter,df,preprocess_extrema)

    def preprocess(self,dfA,dfB):
        dfA["Scenario"] = "A"
        dfB["Scenario"] = "B"
        dfB["Metrique"] = dfB["Metrique"].apply(lambda x: x*1.3 if x > 1000.0 else x)
        df = pd.concat([dfA, dfB], axis=0)

        #Augment data
        df["TempsCalculParPasse"] = df["TempsCalcul"]/df["NombrePasse"]
        df["TempsCalculParVoiture"] = df["TempsCalcul"]/df["NbVoiture"]

        df["TempsCalcul"] = df["TempsCalcul"]*self.performance_time_convert
        df["TempsCalculParVoiture"] = df["TempsCalculParVoiture"]*self.performance_time_convert
        df["TempsCalculParPasse"] = df["TempsCalculParPasse"]*self.performance_time_convert

        df["TotalMinimum"] = df["TotalMinimum"]*self.simulation_time_convert
        #df["NbVoiture"] = int(df["NbVoiture"])
        df["TotalMaximum"] = df["TotalMaximum"]*self.simulation_time_convert
        df["TotalMoyen"] = df["TotalMoyen"]*self.simulation_time_convert
        df["RouteMoyenne"] = df["RouteMoyenne"]*self.simulation_time_convert
        df["AttenteMoyenne"] = df["AttenteMoyenne"]*self.simulation_time_convert
        df["ChargeMoyenne"] = df["ChargeMoyenne"]*self.simulation_time_convert
        df["TempsCalculParVoiture"] = df["TempsCalculParVoiture"]*self.simulation_time_convert

        df["Metrique"] = df["Metrique"].apply(lambda x: x*self.simulation_time_convert if x > 50.0 else x)
        return df

    def preprocess_extrema(self,df):
        #Melting (remove some columns to add data in their place)
        list_all = list(df.columns)
        list_all.remove('TotalMinimum')
        list_all.remove('TotalMaximum')
        list_all.remove('TotalMoyen')
        extrema_df = df.melt(id_vars=list_all, value_vars=['TotalMinimum',"TotalMoyen",'TotalMaximum',],var_name="Extrema",value_name="ValeurExtrema")
        #print(df.head())
        return extrema_df

    def build_new(self):
        #current_df = df.loc[df.NbVoiture == 1]
        all_algo = [
        "Cache"
        ,"PNCSR"
        ,"PNCAR"
        ,"PNCARR"
        ,"PCAPDPCascade"
        ,"PCAPDPPermutation"
        ,"PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ,"PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large"
        ,"PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"
        ]

        cache_algo = ["Cache"]

        pnc_algo = [
        "PNCSR"
        ,"PNCAR"
        ,"PNCARR"     
        ]

        pcs_algo = [
         "PNCARR"
        ,"PCAPDPCascade"
        ,"PCAPDPPermutation"   
        ]

        pcsf_algo = [
         "PCAPFPremier_small"
        ,"PCAPFPremier_medium"
        ,"PCAPFPremier_large"
        ,"PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ,"PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large" 
        ]

        pcsf_premier_algo = [
        "PCAPFPremier_small"
        ,"PCAPFPremier_medium"
        ,"PCAPFPremier_large"
        ]

        pcsf_cascade_algo = [
        "PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ]

        pcsf_permutation_algo = [
        "PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large" 
        ]

        pcsf_petite_fenetre_algo = [
        "PCAPFPremier_small"
        ,"PCAPFCascade_small"
        ,"PCAPFPermutation_small"
        ]

        pcl_algo_r = [
        "PNCARR",
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ]

        pcl_algo = [
        "PNCARR",
        "PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"        
        ]

        pcl_algo_c = [
        "PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"	
        ]

        pcl_algo_nc = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ]

        pcl_algo_g = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"       
        ]

        pcl_algo_ng = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"       
        ]

        potent_algo = [
        #"PNCSR"
        #,"PNCAR"
        "PNCARR"
        ,"PCARLG_PNCAR_C"
        ,"PCAPFPremier_small"
        ]

        #Augment data with time per pass

        #print(~df.Algo.isin(excluded_algo))

        #FONCTIONS
        #Données limites = [PNSCR,PNCAR,PNCARR] [PNCARR]
        #mesure = [temps || une quelconque métrique || temps/passes]
        #Grid 4 bornes, x=[voitures], y =[mesure], hue=[algo]
        #Grid 6 métriques, x=[voitures], y =[mesure], hue=[algo]
        #Grid voiture, x=[borne], y =[mesure], hue=[algo]

        #################HELPERS#######################
        #Colonnes
        #NbBorne,NbVoiture,Algo,NomMetrique,Metrique,
        #TotalMinimum,TotalMaximum,TotalMoyen,RouteMoyenne
        #AttenteMoyenne,ChargeMoyenne,TempsCalcul,NombrePasse
        #TempsCalculParPasse,TempsCalculParVoiture,Scenario
        #################HELPERS#######################
        colonne_penalite = "Penalite"

        #1) Tableau du temps calcul chemins entre les 1000 bornes
        #CANCEL

        #1) Table valeurs de référence
        basic_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            #,"NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            }

        basic_filter_penalite = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            }
        #Densité de borne
        borne_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"Autonomie":["Ioniq"]
            }
        metrique_filter = {
            "NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            ,"Algo":["PNCARR"]
            }

        #SUPPLEMENTAIRE
        #Comparaison  des métriques min-max-moyen avec PNCARR
        self.builder.gxyh_metrique_extrema(
            title = "Temps de déplacement par métrique (algo=PNCARR, voiture=1000 et nombre de bornes=1000)"
            ,grid = "Scenario"
            ,x = "NomMetrique"
            ,y = "ValeurExtrema"
            ,h = "Extrema"
            ,export_name = "Extrema_Metrique"
            ,filters = {"NbVoiture":[50],"NbBorne":[637],"Algo":["PNCARR"]}
            )

        if True:
           
            #2) Temps calcul prétraitement
            self.builder.gxyh(
                title = "Temps de calcul de la carte des bornes augmentée à l'insertion de la ième voiture"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Cache_TempsCalculVoiture"
                ,filters = m(basic_filter,{"Algo":cache_algo})
                )
            #3) Approches non-coopératives
            self.builder.gxyh(
                title = "Comparaison du temps de déplacement des approches non-coopératives"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Algo"
                ,export_name = "Pnc_TempsMoyenDeplacement"
                ,filters = m(basic_filter,{"Algo":pnc_algo})
                )
            self.builder.gxyh(
                title = "Comparaison du temps de calcul des approches non-coopératives"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Pnc_TempsCalcul"
                ,filters = m(basic_filter,{"Algo":pnc_algo})
                )
            #5) Approches simultannées
            self.builder.gxyh(
                title = "Comparaison du temps de déplacement des approches simultannées"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Algo"
                ,export_name = "Ps_TempsMoyenDeplacement"
                ,filters = m(basic_filter,{"Algo":pcs_algo})
                )
            self.builder.gxyh(
                title = "Comparaison du temps de calcul des approches simultannées"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Ps_TempsCalcul"
                ,filters = m(basic_filter,{"Algo":pcs_algo})
                )
            return
            #Pour scénario A et B, les métriques.
            #Deux graphiques sont nécessaires vu les échelles différents (0-1 et l'autre tempos de déplacement)
        self.builder.gxyh(
                title = "Pénalité et pire pénalité, par scénario."
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Penalite"
                ,filters = m(metrique_filter,{"NomMetrique":["Penalite","PirePenalite"]})
                )

        self.builder.gxyh(
                title = "Racine carré de la différence des moyennes des pénalités, par scénario."
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Racine_Penalite"
                ,filters = m(metrique_filter,{"NomMetrique":["RacDiffCarPenalite"]})
                )

        self.builder.gxyh(
                title = "Durée moyenne et pire durée, par scénario."
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Dure"
                ,filters = m(metrique_filter,{"NomMetrique":["DureeMoyenne","PireDuree"]})
                ,order=["DureeMoyenne","PireDuree"]
                )

        self.builder.gxyh(
                title = "Racine carré de la différence des moyennes des durées, par scénario."
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Racine_Duree"
                ,filters = m(metrique_filter,{"NomMetrique":["RacDiffCarDuree"]})
                )
        self.builder.gxyh(
                title = "Temps de calcul des fonctions objectives."
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Temps_Calcul"
                ,filters = metrique_filter
                )
        #9) PCPAF premier - pénalité moyenne
        self.builder.gxyh(
                title = "Pénalités moyennes des approches fenêtrées (combinaison: premier arrivé)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Algo"
                ,export_name = "Comparaison_Penalite_Premier"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_premier_algo})
                )
        self.builder.gxyh(
                title = "Temps de calcul des approches fenêtrées (combinaison: premier arrivé)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Comparaison_TempsCalcul_Premier"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_premier_algo})
                )
        #9b) PCPAF combinaisons - pénalité moyenne
        #TEMPORAIRE le temps que les calculs terminent
        self.builder.gxyh(
                title = "Pénalités moyenne des approches fenêtrées (combinaison: cascade)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Algo"
                ,export_name = "Comparaison_Penalite_Cascade"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_cascade_algo})
                )

        self.builder.gxyh(
                title = "Temps de calcul des approches fenêtrées (combinaison: cascade)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Comparaison_TempsCalcul_Cascade"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_cascade_algo})
                )
        #10) PCPAF avec permutations - pénalité moyenne
        self.builder.gxyh(
                title = "Pénalités moyenne des approches fenêtrées (combinaison: permutation)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Algo"
                ,export_name = "Comparaison_Penalite_Permutation"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_permutation_algo})
                )

        self.builder.gxyh(
                title = "Comparaison du temps de calcul des approches fenêtrées (combinaison: permutation)"
                ,grid = "NbBorne"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Algo"
                ,export_name = "Comparaison_TempsCalcul_Permutation"
                ,filters = m(basic_filter_penalite,{"Algo":pcsf_permutation_algo})
                )

        #12) PCARL + références
        self.builder.gxyh(
            title = "Pénalités des approches basées sur la réparation locale et PNCAR"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Local_Penalite_PNCARR"
            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo_r})
            )
        self.builder.gxyh(
            title = "Temps de calcul des approches basées sur la réparation locale et PNCAR"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Local_TempsCalcul_PNCARR"
            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo_r})
            )

        self.builder.gxyh(
            title = "Pénalités des approches basées sur la réparation locale et PNCAC"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Local_Penalite_PNCAR"
            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo})
            )
        
        self.builder.gxyh(
            title = "Temps de calcul des approches basées sur la réparation locale et PNCAC"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Local_TempsCalcul_PNCAR"
            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo})
            )
        #13) Sommaire: [PNSCR,PNCAR,PNCARR,PCPAFpetit_cascade,PCARL]
        self.builder.gxyh(
            title = "Comparaison des pénalités des approches potentes (scénario A)"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Potente_Penalite_A"
            ,filters = m(basic_filter_penalite,{"Algo":potent_algo})
            )

        self.builder.gxyh(
            title = "Comparaison temps de calcul des approches potentes (scénario A)"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Potente_TempsCalcul_A"
            ,filters = m(basic_filter_penalite,{"Algo":potent_algo})
            )
        #Autonomie
        auto_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"NbBorne":[637]
            }
        self.builder.gxyh(
            title = "Influence de l'autonomie sur la pénalité"
            ,grid = "Autonomie"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Autonomie_Penalite"
            ,filters = m(auto_filter,{"Algo":potent_algo})
            )
        self.builder.gxyh(
            title = "Influence de l'autonomie sur le temps de calcul"
            ,grid = "Autonomie"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Autonomie_TempsCalcul"
            ,filters = m(auto_filter,{"Algo":potent_algo})
            )     
        self.builder.gxyh(
            title = "Influence de la densité de borne sur la pénalité"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Borne_Penalite"
            ,filters = m(borne_filter,{"Algo":potent_algo})
            )
        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul"
            ,filters = m(borne_filter,{"Algo":potent_algo})
            )

        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul"
            ,filters = m(borne_filter,{"Algo":potent_algo,"NbBorne":[45,637]})
            )
        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul_Gros"
            ,filters = m(borne_filter,{"Algo":potent_algo,"NbBorne":[3364]})
            )
        return

        realiste_2 = {
            "Scenario":["B"]
            ,"NomMetrique":["Penalite"]
            ,"NbBorne":[3364]
            ,"Autonomie":["Distribution"]
            }
        self.builder.gxyh(
            title = "Pénalité dans un contexte réaliste futur"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Realiste_Futur_Penalite"
            ,filters = m(realiste_2,{"Algo":potent_algo})
            )
        self.builder.gxyh(
            title = "Temps de calcul dans un contexte réaliste futur"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Realiste_Futur_TempsCalcul"
            ,filters = m(realiste_2,{"Algo":potent_algo})
            )

    def build_trace(self):
        filters = {"TypeValeur":["ValeurMeilleure"]}
        #TODO: on dirait un seul graphique se sauvegarde...
        self.builder.xyh_trace_metrique("plots/Trace_PCAPDPCascade.csv","Trace_PCPDP_Cascade_20")
        #self.builder.xyh_trace_metrique("Trace.csv","Trace_PCPDP_Cascade_Meilleur_20",filters)

    def build_borne(self,borne_file_path):
        self.builder.borne_distribution(borne_file_path,"DistributionDistanceInterBorne")
        self.builder.borne_distribution_segment(borne_file_path,"DistributionNombreSegment")

    def build_all_borne(self):
        self.build_borne("osm.txt")
        self.build_borne("l3.txt")
        self.build_borne("l2l3.txt")

    def build_newest(self):
        all_algo = [
        "Cache"
        ,"PNCSR"
        ,"PNCAR"
        ,"PNCARR"
        ,"PCAPDPCascade"
        ,"PCAPDPPermutation"
        ,"PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ,"PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large"
        ,"PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"
        ]

        cache_algo = ["Cache"]

        pnc_algo = [
        "PNCSR"
        ,"PNCAR"
        ,"PNCARR"     
        ]

        phl_algo = [
        "PNCAR"
        ,"PNCARR"
        ,"PCC"
        ,"PCAPDPCascade"
        ,"PCAPDPPermutation"   
        ]

        pcs_algo = [
            "PNCARR"
        ,"PCAPDPCascade"
        ,"PCAPDPPermutation"   
        ]

        pcsf_algo = [
        "PNCARR"
        ,"PCAPFPremier_small"
        ,"PCAPFPremier_medium"
        ,"PCAPFPremier_large"
        ,"PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ,"PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large" 
        ]

        pcsf_algo_potent = [
        "PNCARR"
        ,"PCAPFPremier_small"
        ,"PCAPFPremier_medium"
        ,"PCAPFPremier_large"
        ]


        pcsf_algo_fail = [
        "PNCARR"
        ,"PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ,"PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large" 
        ]

        pcsf_premier_algo = [
        "PCAPFPremier_small"
        ,"PCAPFPremier_medium"
        ,"PCAPFPremier_large"
        ]

        pcsf_cascade_algo = [
        "PCAPFCascade_small"
        ,"PCAPFCascade_medium"
        ,"PCAPFCascade_large"
        ]

        pcsf_permutation_algo = [
        "PCAPFPermutation_small"
        ,"PCAPFPermutation_medium"
        ,"PCAPFPermutation_large" 
        ]

        pcsf_petite_fenetre_algo = [
        "PCAPFPremier_small"
        ,"PCAPFCascade_small"
        ,"PCAPFPermutation_small"
        ]

        pcarl_algo_all= [
        "PNCARR"
        ,"PCARLG_PNCARR"
        ,"PCARLG_PNCAR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCARR"	
        ,"PCARL_PNCAR"		
        ,"PCARL_PNCARR_C"
        ,"PCARL_PNCAR_C"  
        ]

        pcl_algo_r = [
        "PNCARR"
        ,"PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ]

        pcl_algo = [
        "PNCARR"
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"        
        ]

        pcl_algo_c = [
        "PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"	
        ]

        pcl_algo_nc = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ]

        pcl_algo_g = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"       
        ]

        pcl_algo_ng = [
        "PCARLG_PNCARR"
        ,"PCARL_PNCARR"	
        ,"PCARLG_PNCARR_C" 
        ,"PCARL_PNCARR_C"	
        ,"PCARLG_PNCAR"	
        ,"PCARL_PNCAR"		
        ,"PCARLG_PNCAR_C"	
        ,"PCARL_PNCAR_C"       
        ]

        potent_algo = [
        #"PNCSR"
        #,"PNCAR"
        "PNCARR"
        ,"PCARLG_PNCAR_C"
        ,"PCAPFPremier_small"
        ]

        #Augment data with time per pass

        #print(~df.Algo.isin(excluded_algo))

        #FONCTIONS
        #Données limites = [PNSCR,PNCAR,PNCARR] [PNCARR]
        #mesure = [temps || une quelconque métrique || temps/passes]
        #Grid 4 bornes, x=[voitures], y =[mesure], hue=[algo]
        #Grid 6 métriques, x=[voitures], y =[mesure], hue=[algo]
        #Grid voiture, x=[borne], y =[mesure], hue=[algo]

        #################HELPERS#######################
        #Colonnes
        #NbBorne,NbVoiture,Algo,NomMetrique,Metrique,
        #TotalMinimum,TotalMaximum,TotalMoyen,RouteMoyenne
        #AttenteMoyenne,ChargeMoyenne,TempsCalcul,NombrePasse
        #TempsCalculParPasse,TempsCalculParVoiture,Scenario
        #################HELPERS#######################
        colonne_penalite = "Penalite"

        #1) Tableau du temps calcul chemins entre les 1000 bornes
        #CANCEL
        
        #Table valeurs de référence
        basic_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            #,"NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            }

        basic_filter_small_range = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            #,"NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            ,"NbVoiture":[1, 2, 5, 10,15,20, 30, 40, 50]
            }

        basic_filter_penalite = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            }

        #Densité de borne
        borne_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"Autonomie":["Ioniq"]
            }
        metrique_filter = {
            "NbBorne":[637]
            ,"Autonomie":["Ioniq"]
            ,"Algo":["PCARLG_PNCAR_C"]
            }
            ######Approches hors ligne#####
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Phl_Penalite"
            ,filters = m(basic_filter,{"Algo":phl_algo})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Phl_TempsCalcul"
            ,filters = m(basic_filter,{"Algo":phl_algo})
            ,log_scale = True
            ,log_small_scale= True
            )
        ######Approches en ligne - fenêtres#####
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Pel_Fenetre_Penalite_Potent"
            ,filters = m(basic_filter,{"Algo":pcsf_algo_potent})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Pel_Fenetre_TermpsCalcul_Potent"
            ,filters = m(basic_filter,{"Algo":pcsf_algo_potent})
            ,log_scale = True
            )

        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Pel_Fenetre_Penalite_Fail"
            ,filters = m(basic_filter_small_range,{"Algo":pcsf_algo_fail})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Pel_Fenetre_TermpsCalcul_Fail"
            ,filters = m(basic_filter_small_range,{"Algo":pcsf_algo_fail})
            ,log_scale = True
            )
            #####PCARL#####
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Pcarl_Penalite_PNCAR"
            ,filters = m(basic_filter,{"Algo":pcl_algo_r})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Pcarl_TempsCalcul_PNCAR"
            ,filters = m(basic_filter,{"Algo":pcl_algo_r})
            ,log_scale = True
            )

        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Pcarl_Penalite_PNC"
            ,filters = m(basic_filter,{"Algo":pcl_algo})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Pcarl_TempsCalcul_PNC"
            ,filters = m(basic_filter,{"Algo":pcl_algo})
            ,log_scale = True
            )

        #####Autonomie#####
        auto_filter = {
            "Scenario":["A"]
            ,"NomMetrique":[colonne_penalite]
            ,"NbBorne":[637]
            }
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "Autonomie"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Autonomie_Penalite"
            ,filters = m(auto_filter,{"Algo":potent_algo})
            )
        self.builder.gxyh_single_graphic(
            title = ""
            ,grid = "Autonomie"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Autonomie_TempsCalcul"
            ,filters = m(auto_filter,{"Algo":potent_algo})
            ,log_scale = True
            )  
        return

    #        self.builder.gxyh(
    #            title = "Pénalités des approches basées sur la réparation locale et PNCAC"
    #            ,grid = "NbBorne"
    #            ,x = "NbVoiture"
    #            ,y = "Metrique"
    #            ,h = "Algo"
    #            ,export_name = "Local_Penalite_PNCAR"
    #            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo})
    #            )
    #        
    #        self.builder.gxyh(
    #            title = "Temps de calcul des approches basées sur la réparation locale et PNCAC"
    #            ,grid = "NbBorne"
    #            ,x = "NbVoiture"
    #            ,y = "TempsCalcul"
    #            ,h = "Algo"
    #            ,export_name = "Local_TempsCalcul_PNCAR"
    #            ,filters = m(basic_filter_penalite,{"Algo":pcl_algo})
    #            )
        #####Métriques#####
        self.builder.gxyh(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_PirePenalite"
                ,filters = m(metrique_filter,{"NomMetrique":["PirePenalite"]})
                )

        self.builder.gxyh(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Penalite"
                ,filters = m(metrique_filter,{"NomMetrique":["Penalite"]})
                )

        self.builder.gxyh(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Racine_Penalite"
                ,filters = m(metrique_filter,{"NomMetrique":["RacDiffCarPenalite"]})
                )
        self.builder.gxyh(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_PireDure"
                ,filters = m(metrique_filter,{"NomMetrique":["PireDuree"]})
                )

        self.builder.gxyh(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Dure"
                ,filters = m(metrique_filter,{"NomMetrique":["DureeMoyenne"]})
                )

        self.builder.gxyh_single_graphic(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "Metrique"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Racine_Duree"
                ,filters = m(metrique_filter,{"NomMetrique":["RacDiffCarDuree"]})
                )
        self.builder.gxyh_single_graphic(
                title = ""
                ,grid = "NomMetrique"
                ,x = "NbVoiture"
                ,y = "TempsCalcul"
                ,h = "Scenario"
                ,export_name = "Comparaison_Metrique_Temps_Calcul"
                ,filters = metrique_filter
                ,log_scale = True
                )
        #13) Sommaire: [PNSCR,PNCAR,PNCARR,PCPAFpetit_cascade,PCARL]
        self.builder.gxyh(
            title = "Comparaison des pénalités des approches potentes (scénario A)"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Potente_Penalite_A"
            ,filters = m(basic_filter_penalite,{"Algo":potent_algo})
            )

        self.builder.gxyh(
            title = "Comparaison temps de calcul des approches potentes (scénario A)"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Potente_TempsCalcul_A"
            ,filters = m(basic_filter_penalite,{"Algo":potent_algo})
            )
   
        self.builder.gxyh(
            title = "Influence de la densité de borne sur la pénalité"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Borne_Penalite"
            ,filters = m(borne_filter,{"Algo":potent_algo})
            )
        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul"
            ,filters = m(borne_filter,{"Algo":potent_algo})
            )

        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul"
            ,filters = m(borne_filter,{"Algo":potent_algo,"NbBorne":[45,637]})
            )
        self.builder.gxyh(
            title = "Influence de la densité de borne sur le temps de calcul"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Borne_TempsCalcul_Gros"
            ,filters = m(borne_filter,{"Algo":potent_algo,"NbBorne":[3364]})
            )

        realiste_2 = {
            "Scenario":["B"]
            ,"NomMetrique":["Penalite"]
            ,"NbBorne":[3364]
            ,"Autonomie":["Distribution"]
            }
        self.builder.gxyh(
            title = "Pénalité dans un contexte réaliste futur"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "Metrique"
            ,h = "Algo"
            ,export_name = "Realiste_Futur_Penalite"
            ,filters = m(realiste_2,{"Algo":potent_algo})
            )
        self.builder.gxyh(
            title = "Temps de calcul dans un contexte réaliste futur"
            ,grid = "NbBorne"
            ,x = "NbVoiture"
            ,y = "TempsCalcul"
            ,h = "Algo"
            ,export_name = "Realiste_Futur_TempsCalcul"
            ,filters = m(realiste_2,{"Algo":potent_algo})
            )
