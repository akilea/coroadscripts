import os
import matplotlib.pyplot as plt

class PlotExporter:
    def __init__(self, preview = False,overleaf_image = False,overleaf_folder = "overleaf_plot"):
        self.preview = preview
        self.overleaf_image = overleaf_image
        self.overleaf_folder = overleaf_folder

    def export(self,fig,export_name):
        #if self.preview:
        #    fig.show()
        #if self.overleaf_image:
        #    if not os.path.exists(self.overleaf_folder):
        #        os.mkdir(self.overleaf_folder)
        #    fig.write_image(self.overleaf_folder+"/"+export_name+".pdf")
        if self.overleaf_image:
            fig.savefig(self.overleaf_folder+"/"+export_name+".pdf")

    def exportScatter(self,fig,export_name):
        plt.savefig(self.overleaf_folder+"/"+export_name+".pdf")