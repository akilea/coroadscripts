import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from plotly.colors import n_colors
import numpy as np

#Gray palettes - choose one
cols = 'abcdefghijkl'
greys_all = n_colors('rgb(0, 0, 0)', 'rgb(255, 255, 255)', len(cols)+1, colortype='rgb')
greys_dark = n_colors('rgb(0, 0, 0)', 'rgb(200, 200, 200)', len(cols)+1, colortype='rgb')
greys_light = n_colors('rgb(200, 200, 200)', 'rgb(255, 255, 255)', len(cols)+1, colortype='rgb')
greys = n_colors('rgb(100, 100, 100)', 'rgb(255, 255, 255)', len(cols)+1, colortype='rgb')
theme = "plotly_white"

labels = {
     "NbBorne":"Nombre de bornes"
    ,"NbVoiture":"Nombre de voitures"
    ,"Algo":"Type d'algorithme"
    ,"NomMetrique":"Métrique"
    ,"Metrique":"Métrique"
    ,"TotalMinimum":"Temps de trajet minimum(s)"
    ,"TotalMaximum":"Temps de trajet maximum(s)"
    ,"TotalMoyen":"Temps de trajet moyen(s)"
    ,"RouteMoyenne":"Temps sur la route moyen(s)"
    ,"AttenteMoyenne":"Temps en attente moyen(s)"
    ,"ChargeMoyenne":"Temps de charge moyen(s)"
    ,"TempsCalcul":"Temps de calcul moyen(s)"
    ,"NombrePasse":"Nombre de passe"
}

class PlotBuilder:
    def __init__(self, exporter,df):
        self.exporter = exporter
        self.df = df

        self.simulation_time_convert = 3600.0
        #self.simulation_time_unit = "h"
        self.performance_time_convert = 1.0
        #self.performance_time_unit = "s"

    def plot_general(self,current_df,title,x,y,legend,x_range=None,y_range=None):
        fig = px.scatter(current_df
        , x=x
        , y=y
        ,title=title
        ,color=legend
        ,template=theme
        ,color_discrete_sequence=greys_dark
        ,symbol=legend
        ,size_max =15
        ,range_x=x_range
        ,range_y=y_range
        ,labels=labels
        )
        fig.update_traces(  marker=dict(size=5,
                                line=dict(width=1,
                                    color='DarkSlateGrey'))
                            ,selector=dict(mode='markers')
                            ,textposition='top center'
        )

        fig.update_layout(
            legend=dict(
                x=0.01,
                y=-0.2,
                #traceorder="reversed",
                title_font_family="Times New Roman",
                font=dict(
                    family="Courier",
                    size=10,
                    color="black"
                ),
                #bgcolor="Transparent",
                bordercolor="Black",
                borderwidth=1
            )
            ,legend_orientation="h"
            ,font=dict(
                #family="Courier New, monospace",
                size=10,
                color="Black"
            )
        )
        return fig

    def plot_metrique_voiture(self,title,included,y_value,export_name,average = True):
        current_df = self.df.loc[self.df.Algo.isin(included)]
        current_df[y_value] = current_df[y_value].apply(lambda x: x/self.simulation_time_convert)
        #if average:
        #    #tmp = current_df.groupby(by=["NbBorne","NbVoiture","Algo","NomMetrique"], axis=0,level=0)
        #    #print(tmp.first())
        #    tmpA = current_df.groupby(by=["NbBorne","NbVoiture","Algo","NomMetrique"])
        #    tmpB = tmpA[y_value]
        #    tmpC = tmpB.mean()
        #    tab = pd.DataFrame(np.array(tmpC))
        #    print(np.array(tmpC))
        #    print(tab.head())
        #    tmpD = tmpC.to_frame()
        #    current_df = tmpD
        #    print(current_df)
        #    #print(current_df.to_numpy())
        #    #current_df = current_df.rename(columns = {0:"NbBorne",1:"NbVoiture",2:"Algo",3:"NomMetrique"})
        #    #print(current_df)
        fig = self.plot_general(current_df,title,"NbVoiture",y_value,"Algo",x_range=[0,1500])
        self.exporter.export(fig,export_name)

    def plot_metrique_borne_fix_voiture(self,title,included,y_value,nb_voiture_fixe,export_name):
        current_df = self.df.loc[self.df.Algo.isin(included)]
        current_df = current_df.loc[current_df.NbVoiture == nb_voiture_fixe]
        current_df[y_value] = current_df[y_value].apply(lambda x: x/self.simulation_time_convert)
        fig = self.plot_general(current_df,title,"NbBorne",y_value,"Algo")
        self.exporter.export(fig,export_name)