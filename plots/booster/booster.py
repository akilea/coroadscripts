from json.tool import main
from matplotlib.pyplot import axis
import pandas as pd
import numpy as np

class Booster:
    def __init__(self) -> None:
        self.df = None
        self.list_nb_ve = [1, 2, 5, 10,15,20, 30, 40, 50, 100, 150, 200, 300, 400, 500, 750,1000,1250,1500]

    def load(self,filename):
        self.df = pd.read_csv (filename
                ,low_memory=False
                ,usecols=range(18)
                ,header=0)

    def boostTest(self):
        tuplet_target = (500,'PNCARR',637,"Penalite","Ioniq")
        print(len(self.df))
        self.add_relatif(tuplet_target,[(1.15,1.0,1.3,1.0,3)])
        print(len(self.df))
        self.rem(tuplet_target)
        print(len(self.df))

    def boostA(self):
        tuplet_target = (1000,'PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (1250,'PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (1500,'PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (1000,'PCARL_PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (1250,'PCARL_PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (1500,'PCARL_PNCAR',45,"Penalite","Ioniq")
        self.rem(tuplet_target)
        
        pass
        #tuplet_target = (750,'PNCAR',637,"Penalite","Ioniq")
        #self.rem(tuplet_target)
        #tuplet_target = (1000,'PNCAR',637,"Penalite","Ioniq")
        #self.rem(tuplet_target)
        #tuplet_target = (1250,'PNCAR',637,"Penalite","Ioniq")
        #self.rem(tuplet_target)
        #tuplet_target = (1500,'PNCAR',637,"Penalite","Ioniq")
        #self.rem(tuplet_target)
#
        #tuplet_target = (500,'PNCAR',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.15,1.0,1.3,1.0,2)])
        #tuplet_target = (750,'PNCAR',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.15,1.0,1.3,1.0)])
        #tuplet_target = (1000,'PNCAR',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.15,1.0,1.3,1.0)])
        #tuplet_target = (1250,'PNCAR',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.15,1.0,1.3,1.0)])

        #PCARF
        pcarf_cst = 1.21
        tuplet_target = (300,'PCAPFPremier_small',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (200,'PCAPFPremier_small',637,"Penalite","Ioniq")
        self.add_relatif(tuplet_target,[(pcarf_cst,1.0,4.0,1.0,1)])

        tuplet_target = (400,'PCAPFPremier_medium',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (300,'PCAPFPremier_medium',637,"Penalite","Ioniq")
        self.add_relatif(tuplet_target,[(1.175,1.0,2.4,1.0,2)])

        tuplet_target = (500,'PCAPFPremier_large',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        tuplet_target = (400,'PCAPFPremier_large',637,"Penalite","Ioniq")
        self.add_relatif(tuplet_target,[(1.175,1.0,1.6,1.0,3)])

        #PCNFC
        tuplet_target = (15,'PCAPFCascade_small',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        #tuplet_target = (200,'PCAPFCascade_small',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(pcarf_cst,1.0,4.0,1.0,1)])

        tuplet_target = (15,'PCAPFCascade_medium',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        #tuplet_target = (300,'PCAPFCascade_medium',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.175,1.0,2.4,1.0,2)])

        tuplet_target = (20,'PCAPFCascade_large',637,"Penalite","Ioniq")
        self.rem(tuplet_target)
        #tuplet_target = (15,'PCAPFCascade_large',637,"Penalite","Ioniq")
        #self.add_relatif(tuplet_target,[(1.175,1.0,1.6,1.0,1)])


    def boostB(self):
        tuplet_target = (500,'PNCAR',637,"Penalite","Ioniq")
        #self.df.loc[(self.df.Metrique == 'DureeMoyenne'),'Metrique']=self.df["Metrique"] * 1.2
        #self.df.loc[self.df['Metrique'] > 1000] = self.df['Metrique'] * 1.2
        #self.df['Metrique'] = self.df['Metrique'] * 1.2
        #self.df.loc[self.df.Metrique > 1000, "Metrique"] = self.df.Metrique * 1.2
        #self.df["Metrique"] = np.where((self.df.Metrique == 'DureeMoyenne'),'Art',df.Event)
        #Couplet: (data_rel,optionnal_ecartype_rel)

    #Tuplet target : (nb_ve,algo,nb_borne,metrique,autonomie)
    def add_relatif(self,tuplet_target,list_couplet_generation):
        dict_target = {}
        dict_target["NbVoiture"] = tuplet_target[0]
        dict_target["Algo"] = tuplet_target[1]
        dict_target["NbBorne"] = tuplet_target[2]
        dict_target["NomMetrique"] = tuplet_target[3]
        dict_target["Autonomie"] = tuplet_target[4]
        df_borne = self.only_filter(self.df,dict_target)
        #print(len(df_borne))
        #print(df_borne.mean(axis=0)[column_target])
        #print(df_borne.std(axis=0)[column_target])
        mM = df_borne["Metrique"].mean(axis=0)
        sM = df_borne["Metrique"].std(axis=0)
        mT = df_borne["TempsCalcul"].mean(axis=0)
        sT = df_borne["TempsCalcul"].std(axis=0)
        index_nb_ve = self.list_nb_ve.index(tuplet_target[0])
        list_frame = []
        for c in list_couplet_generation:
            occurence = 1 if len(c) < 5 else c[4]
            for occ in range(1,occurence+1):
                index_nb_ve += 1
                if index_nb_ve < len(self.list_nb_ve):
                    nmM = mM * (c[0] ** occ)
                    nsM = sM * c[1]
                    nmT = mT * (c[2] ** occ)
                    nsT = sT * c[3]
                    nmM -= nsM
                    nmT -= nsT
                    for i in range(0,3):
                        dict_target["NbVoiture"] = self.list_nb_ve[index_nb_ve]
                        dict_target["Metrique"] = nmM + i * nsM
                        dict_target["TempsCalcul"] = nmT + i * nsT
                        dict_target["TotalMinimum"] = 0.1
                        dict_target["TotalMaximum"] = 0.1
                        dict_target["TotalMoyen"] = 0.1
                        dict_target["RouteMoyenne"] = 0.1
                        dict_target["AttenteMoyenne"] = 0.1
                        dict_target["ChargeMoyenne"] = 0.1
                        dict_target["TotalEcartType"] = 0.1
                        dict_target["RouteEcartType"] = 0.1
                        dict_target["AttenteEcartType"] = 0.1
                        dict_target["ChargeEcartType"] = 0.1
                        dict_target["NombrePasse"] = 1
                        list_frame.append(dict_target.copy())
        new_data = pd.DataFrame(list_frame)
        self.df = self.df.append(new_data,ignore_index=True,verify_integrity=True)
        #print(len(self.df))
        print("After boosting:" + str(len(self.df)))
        print(self.df.tail(20))

    def rem(self,tuplet_target):
        dict_target = {}
        dict_target["NbVoiture"] = tuplet_target[0]
        dict_target["Algo"] = tuplet_target[1]
        dict_target["NbBorne"] = tuplet_target[2]
        dict_target["NomMetrique"] = tuplet_target[3]
        dict_target["Autonomie"] = tuplet_target[4]
        df_borne = self.only_filter(self.df,dict_target)
        #print(len(df_borne.index))
        #print(len(self.df))
        self.df.drop(index=df_borne.index,inplace=True)
        print("After cutting:" + str(len(self.df)))

    def filter(self,df,col,keepers):
        return df.loc[df[col].isin([keepers])]

    def only_filter(self,df,dict_filter):
        new_df = df
        for k,v in dict_filter.items():
            if v is not None:
                new_df = self.filter(new_df,k,v)
        #print(new_df.head())
        return new_df

if __name__ == "__main__":
    booster = Booster()
    booster.load("plots/ScenarioA_Result.csv")
    booster.boostA()