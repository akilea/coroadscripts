from math import gamma
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set()
#plt.rcParams.update({"xtick.bottom" : True, "ytick.left" : True})

algo_complete_name = {
	"PCARLG_PNCARR":"PCARL-G-PNCAR-I"
	,"PCARL_PNCARR":"PCARL-S-PNCAR-I"	
	,"PCARLG_PNCARR_C":"PCARL-G-PNCAR-E" 
	,"PCARL_PNCARR_C":"PCARL-S-PNCAR-E"	
	,"PCARLG_PNCAR":"PCARL-G-PNC-I"	
	,"PCARL_PNCAR":"PCARL-S-PNC-I"		
	,"PCARLG_PNCAR_C":"PCARL-G-PNC-E"	
	,"PCARL_PNCAR_C":"PCARL-S-PNC-E"
    ,"Cache":"Cache"
    ,"PNCSR":"PI"
    #,"PNCARR":"PNCTTAR" #Super hack
    ,"PNCAR":"PNC"
    ,"PNCARR":"PNCAR"
    ,"PCAPDPPremier":"PC"
    ,"PCAPDPCascade":"PC-C"
    ,"PCAPDPPermutation":"PC-P"
    ,"PCAPFPremier_small":"PCF-O 1h"
    ,"PCAPFPremier_medium":"PCF-O 2h"
    ,"PCAPFPremier_large":"PCF-O 3h"
    ,"PCAPFCascade_small":"PCF-C 1h"
    ,"PCAPFCascade_medium":"PCF-C 2h"
    ,"PCAPFCascade_large":"PCF-C 3h"
    ,"PCAPFPermutation_small":"PCF-P 1h"
    ,"PCAPFPermutation_medium":"PCF-P 2h"
    ,"PCAPFPermutation_large":"PCF-P 3h"

}

hardcoded_labels = {
         "NbBorne":"Nombre de bornes"
        ,"NbVoiture":"Nombre de VÉ"
        ,"Algo":"Algorithme"
        ,"NomMetrique":"Fonction objective"
        ,"Metrique":"Metrique"
        ,"TotalMinimum":"Durée d'itinéraire' minimum (h)"
        ,"TotalMaximum":"Durée d'itinéraire maximum (h)"
        ,"TotalMoyen":"Durée de l'itinéraire (h)"
        ,"RouteMoyenne":"Temps sur la route moyen (h)"
        ,"AttenteMoyenne":"Temps en attente moyen (h)"
        ,"ChargeMoyenne":"Temps de charge moyen (h)"
        ,"TempsCalcul":"Durée totale de calcul (s)"
        ,"NombrePasse":"Nombre de passe"
        ,"TempsCalculParPasse":"Durée de calcul par fenêtre (s)"
        ,"TempsCalculParVoiture":"Durée de calcul (s)"
        ,"Scenario":"Scenario"
        ,"ValeurExtrema":"Durée moyenne sur la route (h)"
        ,"Extrema":"Interprétation"
    }

metric_label = {
    "Penalite": "Pénalité",
    "PireDuree": "Durée d'itinéraire (h)",
    "RacDiffCarDuree": "Somme de la racine carré diff. des durées (h)",
    "RacDiffCarPenalite": "Somme de la racine carré diff. des pénalités",
    "DureeMoyenne": "Durée d'itinéraire (h)",
    "PirePenalite": "Pénalité"
}

autonomie_label = {
    "Ioniq": "Flotte de Hyunday Ioniq 5",
    "Distribution": "Flotte suivant la distribution de part de marché",
}

def labels(str):
    r = hardcoded_labels[str]
    #if type(r) is dict:
        
    return r

class PlotBuilderSeaBorn:
    def __init__(self, exporter,df,extrema_df):
        self.exporter = exporter
        self.df = df
        self.extrema_df = extrema_df
        print(self.extrema_df.head())

    def m(self,dictA,dictB):
        return {**dictA , **dictB}

    def borne_distribution(self,borne_cache_file,export_name):
        df = pd.read_csv (borne_cache_file
                ,low_memory=False
                ,header=None
                ,delimiter=r"\s+")
        #print(df.head())
        g = sns.displot(df, x=2)
        self.exporter.export(g,export_name)

    def borne_distribution_segment(self,borne_cache_file,export_name):
        df = pd.read_csv (borne_cache_file
                ,low_memory=False
                ,header=None
                ,delimiter=r"\s+")
        #Count occurrence
        occurence = df[1].value_counts(ascending=True)
        occurence_frame = occurence.to_frame()
        print(occurence_frame)
        #df[1].value_counts(ascending=True)
        g = sns.displot(occurence_frame, x=1,bins=100)
        self.exporter.export(g,export_name)


    def xyh_trace_metrique(self,trace_file,export_name, filters = {}):
        df = pd.read_csv (trace_file
                        ,low_memory=False
                        ,usecols=range(4)
                        ,header=0)
        df = self.only_filter(df,filters)
        df["TempsMoyen"] = df["Valeur"]/df["NbVoiture"]
        df["TypeValeur"].replace("NouvelleValeur", "Itération", regex=True,inplace=True)
        df["TypeValeur"].replace("ValeurGardee", "Meilleure", regex=True,inplace=True)
        df["TypeValeur"].replace("PNCARR", "PNCAR", regex=True,inplace=True)
        print(df.head())
        sns.set_theme(style="whitegrid")
        g = sns.scatterplot(data=df, x="Iteration", y="TempsMoyen", hue="TypeValeur",style="TypeValeur"
        , s = 25)
        g.set_xlabel("Itérations")
        g.set_ylabel("Durée du plan (s)")
        g.set(xlim=(0, 250))
        #g.subtitle("Durée d'itinéraire pour PCAVOI-C avec 20 voitures pendant l'exécution.")
        #plt.legend(title='Valeur', labels=['Itération', 'Meilleure',"PNCARR"])
        #g.set_axis_labels("Itération", "Valeur")
        #g.legend.set_title("Valeur pendant calcul")
        #g.tight_layout()
        #g.fig.subplots_adjust(top=0.93)

        self.exporter.exportScatter(g,export_name)


    def gxyh_metrique_extrema(self,title,grid,x,y,h,export_name, filters = {}):
        df = self.filter_all(self.extrema_df,filters)
        print(df.head())
        sns.set_theme(style="whitegrid")
        g = sns.catplot(
            data=df
            ,kind="bar"
            ,col = grid
            ,x=x
            ,y=y
            ,hue=h
            ,ci="sd"
            ,palette="Greys"
            ,alpha=1.0
            ,height=4.5
            ,legend_out = False
            ,col_wrap = 2
        )
        #g.despine(left=True)
        g.set_axis_labels(labels(x), labels(y))
        g.set_xticklabels(rotation=45)
        g.legend.set_title(labels(h))
        g.tight_layout()
        g.fig.subplots_adjust(top=0.93)
        #g.fig.suptitle(title)
        g.set(xlim=(0,None))
        g.set(ylim=(1,None))

        #Exceptions
        if(x == "NbVoiture"):
            for axes in g.axes.flat:
                _ = axes.set_xticklabels(axes.get_xticklabels(), rotation=90)

        self.exporter.export(g,export_name)
        return g

    def gxyh(self,title,grid,x,y,h,export_name, filters = {},order=None,log_scale = False,log_small_scale=False):
        #Separate in that case
        if grid == "NbBorne":
            self.gxyh_single_graphic(title,grid,x,y,h,export_name+"_Petit", self.m(filters,{"NbBorne":[45]}),order,log_scale,log_small_scale)
            self.gxyh_single_graphic(title,grid,x,y,h,export_name+"_Moyen", self.m(filters,{"NbBorne":[637]}),order,log_scale,log_small_scale)
            self.gxyh_single_graphic(title,grid,x,y,h,export_name+"_Grand", self.m(filters,{"NbBorne":[3364]}),order,log_scale,log_small_scale)
        else:
            self.gxyh_single_graphic(title,grid,x,y,h,export_name, filters,order)

    def gxyh_single_graphic(self,title,grid,x,y,h,export_name, filters = {},order=None,log_scale = False,log_small_scale = False ):
        #print(self.df.head())
        df = self.filter_all(self.df,filters)
        #print(df.head())
        #sns.set_theme()
        sns.set_style("whitegrid")
        #sns.set_context("paper")
        #sns.set_style("ticks")
        g = sns.catplot(
            data=df
            ,kind="bar"
            ,col = grid
            ,x=x
            ,y=y
            #,s=1.0
            ,hue=h
            ,ci="sd"
            #,palette="Greys"
            ,alpha=1.0
            ,legend_out = False
            ,col_wrap = 1
            ,col_order=order
            #,scale=0.7
            ,height=5.0
            ,aspect=2.00
            ,errwidth = 1.0
            ,capsize=0.0
            ,log=log_scale
            #,dodge=True
            #,markers=[".","o","v","^","s","P","x","X","d","+",".","o","v"]
            #,markers=["|","1",1,"+","4","x","_","2",0,"3",2,3]
            #,linestyles=['dotted','dashed','dashdot',(0,(1,10)),(0,(5,10)),(0,(3,10,1,10)),(0,(5,1)),'dotted','dashed','dashdot',(0,(1,10))]
            #,facecolors=None
            
        )
        #g.despine(bottom=True)
        #g.despine(offset={"bottom":1})

        label_y = labels(y)
        if(y == "Metrique"):
            print(df["NomMetrique"])
            print(df["NomMetrique"].iloc[0])
            label_y = metric_label[df["NomMetrique"].iloc[0]]

        g.set_axis_labels(labels(x), label_y)
        #g.legend.set_title(labels(h))
        #plt.legend(labels=['Hell Yeh', 'Nah Bruh'])
        g.tight_layout()
        if grid == "NbBorne":
            g.set_titles("Réseau de {col_name} bornes")
        elif y != "TempsCalcul" and grid == "NomMetrique":
            g.set_titles("{col_name}")
        elif grid == "Autonomie":
            g.set_titles("{col_name}")
        #g.fig.subplots_adjust(top=0.93)
        #g.fig.suptitle(title)
        if y == "TempsCalcul" and grid != "NomMetrique":
            #if log_small_scale:
            #    g.set(ylim=(0.001, 310))
            #else:
            #    g.set(ylim=(0.1, 310))
            g.set(ylim=(0.001, 310))
                

        #g.set(ylim=(0, 35))
        #g.set(xlim=(1,1500))
        #g.set(ylim=(0.80,None))
        #them_ticks = plt.xticks()
        #plt.xticks()
        #g.set_titles("") #Au lieu du nb de bornes

        #plt.xticks(["1", "2", "5", "10","15","20", "30", "40", "50", "100", "150", "200", "300", "400", "500", "750","1000","1250","1500"])
        #axes = g.axes.flatten()
        #axes[0].set_ylabel("Number of Defects")
        #axes[0].set_xlabel("Number of Defects")

        #Exceptions
        if(x == "NbVoiture"):
            for axes in g.axes.flat:
                _ = axes.set_xticklabels(axes.get_xticklabels(), rotation=90)

#        if(export_name == "Borne_TempsCalcul"):
#            g.set(ylim=(0, 0.01))


        self.exporter.export(g,export_name)
        return g

    def filter(self,df,col,keepers):
        #print(col)
        #print(keepers)
        return df.loc[df[col].isin(keepers)]

    def filter_all(self,df,dict_filter):
        new_df = df
        for k,v in dict_filter.items():
            if v is not None:
                new_df = self.filter(new_df,k,v)
        new_df["Algo"].replace(algo_complete_name,inplace=True)
        #for k,v in algo_complete_name.items():
        #    new_df["Algo"].replace(k, v, regex=True,inplace=True)
        print(new_df.head())
        return new_df

    def only_filter(self,df,dict_filter):
        new_df = df
        for k,v in dict_filter.items():
            if v is not None:
                new_df = self.filter(new_df,k,v)
        print(new_df.head())
        return new_df

